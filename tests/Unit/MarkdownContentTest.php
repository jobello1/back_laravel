<?php

namespace Tests\Unit;

use App\Library\RouteDataToMarkdown;
use App\Transformers\UserTransformer;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MarkdownContentTest extends TestCase
{
    /**
     * @var array
     */
    private static $route_data_with_body_param = [
        "uri"         => "/register",
        "method"      => "POST",
        "title"       => "Register a user",
        "description" => "Creates a user with a name, a password and an email.",
        "bodyparam"   => [
            "firstname : required|string|max:150",
            "lastname : required|string|max:150",
        ],
        "error"       => "422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above."
    ];

    public function testContentMethod()
    {
        $expected_content = "<span class='badge badge-primary method-badge'>POST</span>";
        $method_content = RouteDataToMarkdown::getMarkdownContentMethod(['method' => 'POST']);
        self::assertEquals($expected_content, $method_content);

        $expected_content = "<span class='badge badge-success method-badge'>PUT</span>";
        $method_content = RouteDataToMarkdown::getMarkdownContentMethod(['method' => 'PUT']);
        self::assertEquals($expected_content, $method_content);
    }

    public function testContentUri()
    {
        $expected_content = "`/register`";
        $uri_content = RouteDataToMarkdown::getMarkdownContentUri(['uri' => '/register']);
        self::assertEquals($expected_content, $uri_content);
    }

    public function testContentTitle()
    {
        $expected_content = "<h2>Register a user</h2>";
        $title_content = RouteDataToMarkdown::getMarkdownContentTitle(['title' => 'Register a user']);
        self::assertEquals($expected_content, $title_content);
    }

    public function testContentName()
    {
        $expected_content = "<code>user.profile</code>";
        $title_content = RouteDataToMarkdown::getMarkdownContentName('user.profile');
        self::assertEquals($expected_content, $title_content);
    }

    public function testContentHeader()
    {
        $expected_content = "<p id='section-1'></p>\n" .
            "<span class='badge badge-primary method-badge'>POST</span>`/register`\n" .
            "<div class='title_n_badge'><h2>Register a user</h2></div>\n" .
            "<div class='id_route'><code>register</code></div>\n";

        $header_content = RouteDataToMarkdown::getMarkdownContentHeader(
            'register',
            self::$route_data_with_body_param,
            1
        );
        self::assertEquals($expected_content, $header_content);
    }

    public function testContentDescription()
    {
        $expected_content = "\nThis is a description";
        $description_content = RouteDataToMarkdown::getMarkdownContentDescription(['description' => 'This is a description']);
        self::assertEquals($expected_content, $description_content);

        $description_content = RouteDataToMarkdown::getMarkdownContentDescription([]);
        self::assertEquals('', $description_content);
    }

    public function testContentPathParam()
    {
        $expected_content = "\n#### Path parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "|<span class='required-param'>title</span>\t|required\t|\n";
        $path_param_content = RouteDataToMarkdown::getMarkdownContentPathParam(['pathparam' => 'title : required']);
        self::assertEquals($expected_content, $path_param_content);

        $expected_content = "\n#### Path parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "|title\t|string, max:150\t|\n";
        $path_param_content = RouteDataToMarkdown::getMarkdownContentPathParam(['pathparam' => 'title : string|max:150']);
        self::assertEquals($expected_content, $path_param_content);

        $path_param_content = RouteDataToMarkdown::getMarkdownContentPathParam([]);
        self::assertEquals('', $path_param_content);
    }

    public function testContentQueryParam()
    {
        $expected_content = "\n#### Query parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "|<span class='required-param'>title</span>\t|required\t|\n";
        $query_param_content = RouteDataToMarkdown::getMarkdownContentQueryParam(['queryparam' => 'title : required']);
        self::assertEquals($expected_content, $query_param_content);

        $expected_content = "\n#### Query parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "|title\t|string, max:150\t|\n";
        $query_param_content = RouteDataToMarkdown::getMarkdownContentQueryParam(['queryparam' => 'title : string|max:150']);
        self::assertEquals($expected_content, $query_param_content);

        $query_param_content = RouteDataToMarkdown::getMarkdownContentQueryParam([]);
        self::assertEquals('', $query_param_content);
    }

    public function testContentRuleClass()
    {
        $expected_content = "\n#### Body parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "| title\t| string, max:200\t|\n" .
            "| content\t| string, max:4000\t|\n" .
            "| job_id\t| int, exists:jobs,id\t|\n";
        $rule_class_content = RouteDataToMarkdown::getMarkdownContentRuleClass([
            'method' => 'PUT',
            'title' => 'Update a question',
            'ruleclass' => 'QuestionRequest'
        ]);
        self::assertEquals($expected_content, $rule_class_content);

        $rule_class_content = RouteDataToMarkdown::getMarkdownContentRuleClass([]);
        self::assertEquals('', $rule_class_content);

        $rule_class_content = RouteDataToMarkdown::getMarkdownContentRuleClass([
            'method' => 'PUT',
            'title' => 'Update a question',
            'ruleclass' => 'QuestionReq'
        ]);
        self::assertEquals('', $rule_class_content);

        $rule_class_content = RouteDataToMarkdown::getMarkdownContentRuleClass([
            'title' => 'Update a question',
            'ruleclass' => 'QuestionRequest'
        ]);
        self::assertEquals('', $rule_class_content);
    }

    public function testContentBodyParam()
    {
        $expected_content = "\n#### Body parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "|<span class='required-param'>title</span>\t|required\t|\n" .
            "|content\t|string, max:150\t|\n";
        $body_param_content = RouteDataToMarkdown::getMarkdownContentBodyParam([
            'bodyparam' => [
                'title : required',
                'content : string|max:150'
            ]
        ]);
        self::assertEquals($expected_content, $body_param_content);

        $expected_content = "\n#### Body parameters\n\n" .
            "|Label|Description|\n" .
            "|:|:|\n" .
            "|title\t|string, max:150\t|\n" .
            "|content\t|string, max:150\t|\n";
        $body_param_content = RouteDataToMarkdown::getMarkdownContentBodyParam([
            'bodyparam' => [
                'title : string|max:150',
                'content : string|max:150'
            ]
        ]);
        self::assertEquals($expected_content, $body_param_content);

        $body_param_content = RouteDataToMarkdown::getMarkdownContentBodyParam([]);
        self::assertEquals('', $body_param_content);
    }

    public function testContentError()
    {
        $expected_content = "\n#### Errors\n\n" .
            "|Code|Description|\n" .
            "|:|:|\n" .
            "|422\t|This is an error\t|\n";
        $error_content = RouteDataToMarkdown::getMarkdownContentError(['error' => '422 : This is an error']);
        self::assertEquals($expected_content, $error_content);

        $error_content = RouteDataToMarkdown::getMarkdownContentError([]);
        self::assertEquals('', $error_content);
    }

    public function testContentResponseTransformer()
    {
        $expected_content = "\n#### Response example\n\n" .
            "<pre><code>{\n" .
            "    \"data\": {\n" .
            "        \"id\": 65,\n" .
            "        \"email\": \"antoine@radioking.com\",\n" .
            "        \"role\": \"student\",\n" .
            "        \"created_at\": \"2020-03-06T12:50:35+00:00\",\n" .
            "        \"updated_at\": \"2020-03-06T12:50:35+00:00\"\n" .
            "    }\n" .
            "}</code></pre>";
        $response_transformer_content = RouteDataToMarkdown::getMarkdownContentResponseTransformer([
            'method' => 'GET',
            'title' => 'Get the current user',
            'responsetransformer' => 'UserTransformer'
        ]);
        self::assertEquals($expected_content, $response_transformer_content);

        $response_transformer_content = RouteDataToMarkdown::getMarkdownContentResponseTransformer([]);
        self::assertEquals('', $response_transformer_content);

        $response_transformer_content = RouteDataToMarkdown::getMarkdownContentResponseTransformer([
            'method' => 'PUT',
            'title' => 'Get the current user',
            'responsetransformer' => 'UserTransfo'
        ]);
        self::assertEquals('', $response_transformer_content);
    }

    public function testContentResponseJsonFile()
    {
        $expected_content = "\n#### Response example\n\n" .
            "<pre><code>{\n" .
            "    \"data\": {\n" .
            "        \"id\": 65,\n" .
            "        \"email\": \"antoine@radioking.com\",\n" .
            "        \"role\": \"student\",\n" .
            "        \"created_at\": \"2020-03-06T12:50:35+00:00\",\n" .
            "        \"updated_at\": \"2020-03-06T12:50:35+00:00\"\n" .
            "    }\n" .
            "}</code></pre>";
        $response_json_file_content = RouteDataToMarkdown::getMarkdownContentResponseJsonFile([
            'method' => 'GET',
            'title' => 'Get the current user',
            'responsejsonfile' => 'get_current_user.json'
        ]);
        self::assertEquals($expected_content, $response_json_file_content);

        $response_json_file_content = RouteDataToMarkdown::getMarkdownContentResponseJsonFile([]);
        self::assertEquals('', $response_json_file_content);

        $response_json_file_content = RouteDataToMarkdown::getMarkdownContentResponseJsonFile([
            'method' => 'PUT',
            'title' => 'Get the current user',
            'responsejsonfile' => 'get_current.json'
        ]);
        self::assertEquals('', $response_json_file_content);
    }
}
