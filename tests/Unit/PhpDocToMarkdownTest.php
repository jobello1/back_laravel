<?php

namespace Tests\Unit;

use App\Library\PhpDocToMarkdown;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PhpDocToMarkdownTest extends TestCase
{
    /**
     * @return void
     * @throws \Exception
     */
    public function testGenerate()
    {
        Storage::fake('docs');

        PhpDocToMarkdown::generate();
        $release = config('larecipe.versions.default');

        foreach (Storage::disk('routes')->files('api') as $route_file_path) {
            $file_index = rtrim(substr(
                $route_file_path,
                strrpos($route_file_path, '/') + 1
                ), ".php");
            Storage::disk('docs')->assertExists("$release/$file_index.md");
        }
    }
}
