<?php

namespace Tests\Feature;

use App\UserProfile;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGetAuthenticatedUser()
    {
        $this->actingAs(self::$user_pro, 'api');

        $this->json('GET', '/user')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'email',
                    'role',
                    'created_at',
                    'updated_at'
                ]
            ])
            ->assertJson([
                'data' => [
                    'id' => self::$user_pro->id,
                    'email' => self::$user_pro->email,
                    'role' => self::$user_pro->profile->role,
                ]
            ]);
    }
}
