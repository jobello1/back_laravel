<?php

namespace Tests\Feature;

use App\User;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class AuthTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRegister()
    {
        $this->json('POST', '/register', [
            'firstname' => 'Pro',
            'lastname'  => 'Prof',
            'role'      => UserProfile::ROLE_PROFESSIONAL,
            'birth_date' => self::$faker->dateTimeBetween('-30 years', '-22 years')->format('Y-m-d'),
            'email'     => ($email = self::$faker->email),
            'password' => 'xxxxxx',
            'password_confirmation' => 'xxxxxx',
            'job' => self::$faker->jobTitle
        ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'token_type',
                'expires_in',
                'access_token',
                'refresh_token'
            ]);

        $user = User::where('email', $email)->first();

        self::assertNotNull($user);
        self::assertTrue(Hash::check('xxxxxx', $user->password));

        $profile = $user->profile;
        $desc_note = $profile->descriptionNote;

        self::assertNotNull($profile);
        self::assertTrue($profile->user_id === $user->id);
        self::assertTrue($profile->role === UserProfile::ROLE_PROFESSIONAL);

        self::assertNotNull($desc_note);
        self::assertTrue($desc_note->user_profile_id === $profile->id);
        self::assertTrue($desc_note->title === 'À propos de ' . $profile->firstname);
    }

    public function testRegisterWithMissingParams()
    {
        // Missing body parameter
        $this->json('POST', '/register')
            ->assertStatus(422);
    }

    public function testLogin()
    {
        $this->json('POST', '/login', [
            'email' => self::$user_pro->email,
            'password' => 'xxxxxx'
        ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'token_type',
                'expires_in',
                'access_token',
                'refresh_token'
            ]);
    }
}
