<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserProfileTest extends TestCase
{
    public function testGetMyProfile()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('GET', '/myProfile')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'firstname',
                    'lastname',
                    'birth_date',
                    'city',
                    'post_code',
                    'job',
                    'descriptionNote',
                    'optionalNotes'
                ]
            ])
            ->assertJson([
                'data' => [
                    'id' => self::$user_pro->profile->id,
                    'user_id' => self::$user_pro->id,
                ]
            ]);
    }

    public function testGetAProfile()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('GET', '/users/' . self::$user_student->id . '/profile')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'user_id',
                    'firstname',
                    'lastname',
                    'birth_date',
                    'formation',
                    'descriptionNote',
                    'optionalNotes'
                ]
            ])
            ->assertJson([
                'data' => [
                    'id' => self::$user_student->profile->id,
                    'user_id' => self::$user_student->id,
                ]
            ]);
    }


    public function testProUserModifyProfile()
    {
        $this->actingAs(self::$user_pro, 'api');

        $data = [
            'firstname' => 'I am a',
            'lastname'  => 'Professional',
            'job'       => 'Office manager'
        ];

        // Has access to job field
        $this->json('PUT', '/users/' . self::$user_pro->id . '/profile', $data)
            ->assertStatus(200)
            ->assertJson([
                'data' => $data
            ]);

        // Has no access to formation field
        $this->json('PUT', '/users/' . self::$user_pro->id . '/profile', [
            'formation' => 'Engineer School'
        ])
            ->assertStatus(200);

        $this->assertDatabaseHas('user_profiles', [
            'user_id' => self::$user_pro->id,
            'formation' => null
        ]);
    }

    public function testStudentUserModifyProfile()
    {
        $this->actingAs(self::$user_student, 'api');

        $data = [
            'firstname' => 'I am a',
            'lastname'  => 'Professional',
            'formation' => 'Engineer School'
        ];

        // Has access to formation field
        $this->json('PUT', '/users/' . self::$user_student->id . '/profile', $data)
            ->assertStatus(200)
            ->assertJson([
                'data' => $data
            ]);

        // Has no access to job field
        $this->json('PUT', '/users/' . self::$user_student->id . '/profile', [
            'job'       => 'Office manager'
        ])
            ->assertStatus(200);

        $this->assertDatabaseHas('user_profiles', [
            'user_id' => self::$user_student->id,
            'job'     => null
        ]);
    }

    public function testUnauthorizedUserCantModifyProfile()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('PUT', '/users/' . self::$user_student->id . '/profile')
            ->assertStatus(403);
    }
}
