<?php

namespace Tests\Feature;

use App\Job;
use App\Question;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class QuestionTest extends TestCase
{
    /**
     * @var Job
     */
    private static $arch_job;
    /**
     * @var Job
     */
    private static $chef_job;
    /**
     * @var Question
     */
    private static $arch_question;
    /**
     * @var Question
     */
    private static $chef_question;
    /**
     * @var User
     */
    private static $user_student2;

    public function setUp(): void
    {
        parent::setUp();
        self::$arch_job = Job::where('name', 'Architecte')->first();
        self::$chef_job = Job::where('name', 'Chef de projet')->first();
        self::$user_student2 = factory(User::class)->state('role_student')->create();

        self::$arch_question = self::$user_student->profile->questions()->save(factory(Question::class)->make([
            'job_id' => self::$arch_job->id
        ]));
        self::$arch_question = self::$user_student->profile->questions()->save(factory(Question::class)->make([
            'job_id' => self::$arch_job->id
        ]));
        self::$chef_question = self::$user_student2->profile->questions()->save(factory(Question::class)->make([
            'job_id' => self::$chef_job->id
        ]));
    }

    public function testIndexByUser()
    {
        $this->actingAs(self::$user_student, 'api');
        $this->json('GET', '/users/' . self::$user_student->id . '/questions')
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'content',
                        'created_at',
                        'userProfile'
                    ]
                ]
            ])
            ->assertJsonCount(2, 'data');

        $this->actingAs(self::$user_student2, 'api');
        $this->json('GET', '/users/' . self::$user_student2->id . '/questions')
            ->assertStatus(200)
            ->assertJsonCount(1, 'data');
    }

    public function testUnauthorizedUserCantGetQuestions()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('GET', '/users/' . self::$user_student->id . '/questions')
            ->assertStatus(403);
    }

    public function testIndexByJob()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('GET', '/jobs/' . self::$arch_job->id . '/questions', [
            'per_page' => 2
        ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'title',
                        'content',
                        'created_at',
                        'userProfile'
                    ]
                ]
            ])
            ->assertJsonCount(2, 'data');
    }

    public function testCreateQuestion()
    {
        $this->actingAs(self::$user_student, 'api');

        $job = Job::where('name', 'Architecte')->first();
        $this->json('POST', '/users/' . self::$user_student->id . '/questions', [
            'title' => self::$faker->text(200),
            'content' => self::$faker->text(4000),
            'job_id' => $job->id
        ])
            ->assertStatus(200);

        // Pro can not create questions
        $this->actingAs(self::$user_pro, 'api');

        $job = Job::where('name', 'Architecte')->first();
        $this->json('POST', '/users/' . self::$user_pro->id . '/questions', [
            'title' => self::$faker->text(200),
            'content' => self::$faker->text(4000),
            'job_id' => $job->id
        ])
            ->assertStatus(403);
    }

    public function testUnauthorizedUserCantCreateQuestion()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('POST', '/users/' . self::$user_student->id . '/questions')
            ->assertStatus(403);
    }

    public function testGetSingleQuestion()
    {
        $this->actingAs(self::$user_pro, 'api');
        $this->json('GET', '/questions/' . self::$arch_question->id)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'content',
                    'created_at',
                    'userProfile',
                    'job'
                ]
            ]);
    }

    public function testModifyQuestion()
    {
        $this->actingAs(self::$user_student2, 'api');
        $this->json('PUT', '/questions/' . self::$chef_question->id, [
            'title' => $title = self::$faker->text(200),
            'content' => $content = self::$faker->text(4000),
        ])
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'title',
                    'content',
                    'created_at',
                    'userProfile',
                    'job'
                ]
            ])
            ->assertJson([
                'data' => [
                    'id' => self::$chef_question->id,
                    'title' => $title,
                    'content' => $content
                ]
            ]);
    }

    public function testUnauthorizedUserCantModifyQuestion()
    {
        $this->actingAs(self::$user_student, 'api');
        $this->json('PUT', '/questions/' . self::$chef_question->id)
            ->assertStatus(403);
    }

    public function testDeleteQuestion()
    {
        $this->actingAs(self::$user_student2, 'api');
        $this->json('DELETE', '/questions/' . self::$chef_question->id)
            ->assertStatus(204);
    }
}
