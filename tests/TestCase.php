<?php

namespace Tests;

use App\User;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use DatabaseTransactions;

    /**
     * @var \Faker\Generator
     */
    protected static $faker;

    /**
     * @var User
     */
    protected static $user_pro;

    /**
     * @var User
     */
    protected static $user_student;

    public function setUp() : void
    {
        parent::setUp();

        if (!isset(self::$faker)) {
            self::$faker = \Faker\Factory::create();
        }

        self::$user_pro = factory(User::class)->state('role_pro')->create();
        self::$user_student = factory(User::class)->state('role_student')->create();

        $this->seed('JobSeeder');
    }
}
