<?php

/**
 * @title Get all questions of a user
 * @description Get all the questions that was created by a specific user.
 *
 * @pathparam user : A user id
 *
 * @responsejsonfile get_all_user_questions.json
 */
Route::get('users/{user}/questions', 'QuestionController@indexByUser')
    ->name('questions.indexByUser')
    ->middleware('isAuthorized:user');

/**
 * @title Get all questions of a job
 * @description Get all the questions that was created for a specific job. Results are paginated, see query parameters.
 *
 * @pathparam job : A job id
 * @queryparam per_page : How much element you want per page
 * @queryparam page : Which page you want to get
 *
 * @responsejsonfile get_all_job_questions.json
 */
Route::get('jobs/{job}/questions', 'QuestionController@indexByJob')
    ->name('questions.indexByJob');

/**
 * @title Create a question
 * @description Create a question for a specific user and a job.
 *
 * @pathparam user : A user id
 *
 * @ruleclass QuestionRequest
 * @responsetransformer QuestionTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::post('/users/{user}/questions' ,'QuestionController@store')
    ->name('questions.store')
    ->middleware('isAuthorized:user')
    ->middleware('hasGoodRole:0');

/**
 * @title Get a question
 *
 * @pathparam question : A question id
 *
 * @responsetransformer QuestionTransformer
 */
Route::get('/questions/{question}', 'QuestionController@show')
    ->name('questions.show');

/**
 * @title Update a question
 *
 * @pathparam question : A question id
 *
 * @ruleclass QuestionRequest
 * @responsetransformer QuestionTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::put('/questions/{question}', 'QuestionController@update')
    ->name('questions.update')
    ->middleware('isAuthorized:question.userProfile.user')
    ->middleware('hasGoodRole:0');

/**
 * @title Delete a question
 * @description Returns 204 if success.
 *
 * @pathparam question : A question id
 */
Route::delete('/questions/{question}', 'QuestionController@destroy')
    ->name('questions.destroy')
    ->middleware('isAuthorized:question.userProfile.user')
    ->middleware('hasGoodRole:0');

/**
 * @title Create an answer
 * @description Add an answer to a specific question.
 *
 * @pathparam question : A question id
 *
 * @ruleclass AnswerRequest
 * @responsetransformer QuestionTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 * @error 400 : This question already has an answer.
 */
Route::post('/questions/{question}/answers' ,'AnswerController@store')
    ->name('answers.store')
    ->middleware('hasGoodRole:1');

/**
 * @title Update an answer
 *
 * @pathparam answer : An answer id
 *
 * @ruleclass AnswerRequest
 * @responsetransformer QuestionTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::put('/answers/{answer}' ,'AnswerController@update')
    ->name('answers.update')
    ->middleware('isAuthorized:answer.userProfile.user')
    ->middleware('hasGoodRole:1');

/**
 * @title Delete an answer
 * @description Returns 204 if success.
 *
 * @pathparam answer : An answer id
 */
Route::delete('/answers/{answer}' ,'AnswerController@destroy')
    ->name('answers.destroy')
    ->middleware('isAuthorized:answer.userProfile.user')
    ->middleware('hasGoodRole:1');
