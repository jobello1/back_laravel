<?php

/**
 * @title Get current profile
 * @description Get the profile of the current authenticated user
 *
 * @responsetransformer UserProfileTransformer
 */
Route::get('/myProfile', 'UserProfileController@myProfile')
    ->name('user.profile.me');

/**
 * @title Get a profile
 * @description Get the profile of a user
 *
 * @pathparam user : A user id
 *
 * @responsetransformer UserProfileTransformer
 */
Route::get('/users/{user}/profile', 'UserProfileController@show')
    ->name('user.profile');

/**
 * @title Modify a profile
 * @description Modify the profile of a user
 *
 * @pathparam user : A user id
 *
 * @bodyparam firstname : string|max:150
 * @bodyparam lastname : string|max:150
 * @bodyparam birth_date : date|before:today
 * @bodyparam city : string|max:150
 * @bodyparam postcode : string|regex:/[0-9]{5}/
 * @bodyparam job : string|max:150|<b>Only available if the user is a professional</b>
 * @bodyparam formation : string|max:150|<b>Only available if the user is a student</b>
 *
 * @responsetransformer UserProfileTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::put('/users/{user}/profile', 'UserProfileController@update')
    ->name('user.profile.update')
    ->middleware('isAuthorized:user');
