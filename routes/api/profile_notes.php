<?php

/**
 * @title Modify a description note
 * @description Modify a description note of a profile.
 *
 * @pathparam user : A user id
 *
 * @ruleclass NoteRequest : ProfileDescriptionNote
 * @responsetransformer UserProfileTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::put('/users/{user}/notes/description', 'NoteController@modifyDescriptionNote')
    ->name('user.profile.notes.description.update')
    ->middleware('isAuthorized:user');

/**
 * @title Add an optional note
 * @description Add an optional note to a profile.
 *
 * @pathparam user : A user id
 *
 * @ruleclass NoteRequest : ProfileOptionalNote
 * @responsetransformer UserProfileTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::post('/users/{user}/notes/optional' ,'NoteController@addOptionalNote')
    ->name('user.profile.notes.optional.create')
    ->middleware('isAuthorized:user');

/**
 * @title Delete an optional note
 * @description Delete an optional note from a profile. Returns 204 if success.
 *
 * @pathparam note : A note id
 */
Route::delete('notes/optional/{note}' ,'NoteController@removeOptionalNote')
    ->name('user.profile.notes.optional.destroy')
    ->middleware('isAuthorized:note.userProfile.user');

/**
 * @title Modify an optional note
 * @description Modify an optional note from a profile.
 *
 * @pathparam note : A note id
 *
 * @ruleclass NoteRequest : ProfileOptionalNote
 * @responsetransformer UserProfileTransformer
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::put('notes/optional/{note}' ,'NoteController@modifyOptionalNote')
    ->name('user.profile.notes.optional.update')
    ->middleware('isAuthorized:note.userProfile.user');
