<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * @title Register a user
 * @description Creates a user with a name, a password and an email.
 *
 * @bodyparam firstname : required|string|max:150
 * @bodyparam lastname : required|string|max:150
 * @bodyparam role : required|in:student,professional
 * @bodyparam birth_date : required|date|before now
 * @bodyparam email : required|string|email|max:190|unique:users
 * @bodyparam password : required|string|min:6
 * @bodyparam password_confirmation : required|string|min:6|same:password
 * @bodyparam job : required|string|max:150|<b>Only required if the role field is professional</b>
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::post('/register', 'Auth\RegisterController@register')
    ->name('register');

/**
 * @title Login a user
 * @description Authenticates a user based on email and password.
 *
 * @bodyparam email : required|string
 * @bodyparam password : required|string
 *
 * @error 422 : Unprocessable entity. The parameters you gave (in query or body) don't fit to requirements specified above.
 */
Route::post('/login', 'Auth\LoginController@login')
    ->name('login');

Route::middleware('auth:api')->group(static function () {
    /**
     * @title Get the current authenticated user
     *
     * @responsetransformer UserTransformer
     * @error 401 : Unauthenticated. You must login first.
     */
    Route::get('/user', 'Auth\UserController@me')
        ->name('user.show');

    #
    # User profiles
    #
    require base_path('routes/api/user_profile.php');

    #
    # Profile notes
    #
    require base_path('routes/api/profile_notes.php');

    #
    # Questions
    #
    require base_path('routes/api/questions.php');
});

/**
 * @title Get all jobs
 * @description Get all jobs and their linked jobs.
 *
 * @responsejsonfile get_jobs.json
 */
Route::get('/jobs', 'JobController@index')
    ->name('jobs');
