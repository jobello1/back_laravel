<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\User;
use App\UserProfile;
use Faker\Generator as Faker;

$factory->define(User::class, function (Faker $faker) {
    return [
        'email' => $faker->email,
        'password' => \Illuminate\Support\Facades\Hash::make('xxxxxx')
    ];
});

$factory->state(User::class, 'role_student', function ($faker) {
    return [];
});

$factory->state(User::class, 'role_pro', function ($faker) {
    return [];
});

$factory->afterCreatingState(User::class, 'role_student',function (User $user, Faker $faker) {
    $user->profile()->save(factory(UserProfile::class)->create([
        'user_id' => $user->id,
        'role' => UserProfile::ROLE_STUDENT
    ]));
});

$factory->afterCreatingState(User::class, 'role_pro',function (User $user, Faker $faker) {
    $user->profile()->save(factory(UserProfile::class)->create([
        'user_id' => $user->id,
        'role' => UserProfile::ROLE_PROFESSIONAL,
        'job'  => $faker->jobTitle
    ]));
});
