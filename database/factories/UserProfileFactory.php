<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UserProfile;
use Faker\Generator as Faker;

$factory->define(UserProfile::class, function (Faker $faker) {
    return [
        'firstname' => $faker->firstName,
        'lastname'  => $faker->lastName,
        'birth_date' => $faker->dateTimeBetween('-30 years', '-16 years')
    ];
});

$factory->afterCreating(UserProfile::class, function (UserProfile $profile, Faker $faker) {
    $profile->descriptionNote()->create([
        'title' => 'À propos de ' . $profile->firstname,
    ]);
});
