<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_profile_id')->unsigned()->index();
            $table->integer('job_id')->unsigned()->index();
            $table->string('title', 200);
            $table->string('content', 4000);

            $table->timestamps();

            $table->foreign('user_profile_id')
                ->references('id')->on('user_profiles')
                ->onDelete('cascade');
            $table->foreign('job_id')
                ->references('id')->on('jobs');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}
