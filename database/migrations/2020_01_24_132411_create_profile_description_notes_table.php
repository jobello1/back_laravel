<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \App\ProfileNote;

class CreateProfileDescriptionNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_description_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_profile_id')->unsigned()->index();
            $table->string('title', 200);
            $table->string('text', 4000)->nullable();
            $table->timestamps();

            $table->foreign('user_profile_id')
                ->references('id')->on('user_profiles')
                ->onDelete('cascade');

            $table->unique('user_profile_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_description_notes');
    }
}
