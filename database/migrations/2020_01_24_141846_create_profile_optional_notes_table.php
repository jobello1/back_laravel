<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfileOptionalNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_optional_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_profile_id')->unsigned()->index();
            $table->string('title', 200);
            $table->string('text', 4000)->nullable();
            $table->timestamps();

            $table->foreign('user_profile_id')
                ->references('id')->on('user_profiles')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profile_optional_notes');
    }
}
