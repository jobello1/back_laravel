<?php

use App\Job;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class JobSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        Job::truncate();
        DB::table('linked_jobs')->truncate();
        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');

        $jobs = [
            'Architecte',
            'Chef de chantier',
            'Ergonomiste',
            'Agriculteur',
            'Caissier',
            'Technicien de surface',
            'Développeur web',
            'Administrateur réseau',
            'Chef de projet',
        ];

        $links = [
            'Architecte' => 'Chef de chantier',
            'Caissier' => 'Technicien de surface',
            'Développeur web' => 'Administrateur réseau',
            'Développeur web' => 'Chef de Projet',
            'Chef de projet' => 'Administrateur réseau'
        ];

        foreach ($jobs as $job_name) {
            Job::create(['name' => $job_name]);
        }

        foreach ($links as $name1 => $name2) {
            $job1 = Job::where('name', $name1)->firstOrFail();
            $job2 = Job::where('name', $name2)->firstOrFail();

            $job1->linkedJobs()->attach($job2);
            $job2->linkedJobs()->attach($job1);
        }
    }
}
