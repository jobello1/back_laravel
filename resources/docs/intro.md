# Intro

This is the documentation of Jobello's back-end.

### Notes

All rules that can be found in description rows of parameters tables (such as body parameters, query paramters..) are detailed [here](https://laravel.com/docs/5.2/validation#available-validation-rules).
