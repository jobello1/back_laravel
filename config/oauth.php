<?php

return [
    'client_id'     => env('OAUTH_CLIENT_ID'),
    'client_secret' => env('OAUTH_CLIENT_SECRET'),
    'expiration'    => [
        'access_token'  => env('ACCESS_TOKEN_EXPIRY_SECONDS', 900),
        'refresh_token' => env('REFRESH_TOKEN_EXPIRY_SECONDS', 2592000)
    ],
];
