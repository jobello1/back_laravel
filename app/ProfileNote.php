<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProfileNote extends Model
{
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'text'
    ];

    public function userProfile()
    {
        return $this->belongsTo(UserProfile::class);
    }
}
