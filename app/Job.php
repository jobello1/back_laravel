<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    const WITH_RELATIONS = ['linkedJobs'];
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name'];

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function linkedJobs()
    {
        return $this->belongsToMany(Job::class, 'linked_jobs', 'job_id', 'linked_job_id');
    }
}
