<?php

namespace App\Transformers;

use App\Answer;
use League\Fractal\TransformerAbstract;

class AnswerTransformer extends TransformerAbstract
{
    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $defaultIncludes = Answer::WITH_RELATIONS;

    /**
     * A Fractal transformer.
     *
     * @param Answer $answer
     * @return array
     */
    public function transform(Answer $answer)
    {
        return [
            'id' => $answer->id,
            'content' => $answer->content,
            'created_at' => optional($answer->created_at)->format(\DateTime::ATOM),
        ];
    }

    public function includeUserProfile(Answer $answer)
    {
        $author = $answer->userProfile;

        return $this->item($author, new UserProfileTransformer());
    }
}
