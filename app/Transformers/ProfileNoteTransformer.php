<?php

namespace App\Transformers;

use App\ProfileNote;
use League\Fractal\TransformerAbstract;

class ProfileNoteTransformer extends TransformerAbstract
{
    /**
     * A Fractal transformer.
     *
     * @param ProfileNote $note
     * @return array
     */
    public function transform(ProfileNote $note)
    {
        return [
            'id'         => $note->id,
            'title'      => $note->title,
            'text'       => $note->text,
            'created_at' => $note->created_at,
            'updated_at' => $note->updated_at
        ];
    }
}
