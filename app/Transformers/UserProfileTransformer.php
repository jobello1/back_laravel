<?php

namespace App\Transformers;

use App\UserProfile;
use League\Fractal\TransformerAbstract;

class UserProfileTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $availableIncludes = UserProfile::WITH_RELATIONS;

    /**
     * A Fractal transformer.
     *
     * @param UserProfile $user_profile
     * @return array
     */
    public function transform(UserProfile $user_profile)
    {
        $return_array = [
            'id'          => $user_profile->id,
            'user_id'     => $user_profile->user_id,
            'firstname'   => $user_profile->firstname,
            'lastname'    => $user_profile->lastname,
            'birth_date'  => $user_profile->birth_date->format(\DateTime::ATOM),
            'created_at'  => optional($user_profile->created_at)->format(\DateTime::ATOM),
            'updated_at'  => optional($user_profile->updated_at)->format(\DateTime::ATOM),
        ];
        if ($user_profile->role == UserProfile::ROLE_PROFESSIONAL) {
            $return_array['city']= $user_profile->city;
            $return_array['post_code']= $user_profile->post_code;
            $return_array['job']= $user_profile->job;
        } else if ($user_profile->role == UserProfile::ROLE_STUDENT) {
            $return_array['formation'] = $user_profile->formation;
        }

        return $return_array;
    }

    public function includeDescriptionNote(UserProfile $profile)
    {
        $description_note = $profile->descriptionNote;

        return $this->item($description_note, new ProfileNoteTransformer());
    }

    public function includeOptionalNotes(UserProfile $profile)
    {
        $optional_notes = $profile->optionalNotes;

        return $this->collection($optional_notes, new ProfileNoteTransformer());
    }
}
