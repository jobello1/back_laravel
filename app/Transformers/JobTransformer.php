<?php

namespace App\Transformers;

use App\Job;
use League\Fractal\TransformerAbstract;

class JobTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = [];

    /**
     * List of resources possible to include
     *
     * @var array
     */
    protected $availableIncludes = Job::WITH_RELATIONS;

    /**
     * A Fractal transformer.
     *
     * @param Job $job
     * @return array
     */
    public function transform(Job $job)
    {
        return [
            'id'         => $job->id,
            'name'       => $job->name,
        ];
    }

    public function includeLinkedJobs(Job $job)
    {
        $linked_jobs = $job->linkedJobs;

        return $this->collection($linked_jobs, new JobTransformer());
    }
}
