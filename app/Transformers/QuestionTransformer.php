<?php

namespace App\Transformers;

use App\Question;
use League\Fractal\TransformerAbstract;

class QuestionTransformer extends TransformerAbstract
{
    /**
     * List of resources to automatically include
     *
     * @var array
     */
    protected $defaultIncludes = Question::WITH_RELATIONS;

    /**
     * A Fractal transformer.
     *
     * @param Question $question
     * @return array
     */
    public function transform(Question $question)
    {
        return [
            'id' => $question->id,
            'title' => $question->title,
            'content' => $question->content,
            'created_at' => optional($question->created_at)->format(\DateTime::ATOM),
        ];
    }

    public function includeUserProfile(Question $question)
    {
        $author = $question->userProfile;

        return $this->item($author, new UserProfileTransformer());
    }

    public function includeAnswer(Question $question)
    {
        $answer = $question->answer;

        return $answer ? $this->item($answer, new AnswerTransformer()) : null;
    }

    public function includeJob(Question $question)
    {
        $job = $question->job;

        return $this->item($job, new JobTransformer());
    }
}
