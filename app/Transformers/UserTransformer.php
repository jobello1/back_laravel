<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    /**
     * @param User $user
     * @return array
     */
    public function transform(User $user)
    {
        return [
            'id'         => $user->id,
            'email'      => $user->email,
            'role'       => $user->profile->role,
            'created_at' => optional($user->created_at)->format(\DateTime::ATOM),
            'updated_at' => optional($user->updated_at)->format(\DateTime::ATOM),
        ];
    }
}
