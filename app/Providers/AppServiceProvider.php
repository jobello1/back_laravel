<?php

namespace App\Providers;

use BinaryTorch\LaRecipe\Facades\LaRecipe;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        LaRecipe::style('boostrap', public_path("packages/custom_css/larecipe_custom.css"));
    }
}
