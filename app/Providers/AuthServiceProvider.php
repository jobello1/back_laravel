<?php

namespace App\Providers;

use App\User;
use App\UserProfile;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes(function ($router) /** @var \Laravel\Passport\RouteRegistrar $router */ {
            $router->forAccessTokens();
        });

        Passport::tokensExpireIn(now()->addSeconds(config('oauth.expiration.access_token')));

        Passport::refreshTokensExpireIn(now()->addSeconds(config('oauth.expiration.refresh_token')));

        Gate::define('isOwner', static function (User $user_to_grant, User $resource_owner) {
            return $resource_owner && $resource_owner->id === $user_to_grant->id;
        });

        Gate::define('hasGoodRole', static function (User $user_to_grant, $role_to_have) {
            return $user_to_grant->profile && $user_to_grant->profile->role === UserProfile::ROLES[$role_to_have];
        });
    }
}
