<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    const WITH_RELATIONS = ['userProfile'];
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['content', 'user_profile_id'];

    public function userProfile()
    {
        return $this->belongsTo(UserProfile::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }
}
