<?php

namespace App\Traits;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

trait ProxyToken
{
    /**
     * @param Request $request
     * @param string  $grantType
     * @param array   $params
     * @param string  $scope
     * @return \Illuminate\Http\JsonResponse
     */
    public static function getToken(Request $request, $grantType, array $params = [], $scope = null)
    {
        $params += [
            'grant_type'    => $grantType,
            'scope'         => $scope,
        ];

        $request->request->add($params);
        $proxy = Request::create('oauth/token', 'POST');
        return Route::dispatch($proxy);
    }
}
