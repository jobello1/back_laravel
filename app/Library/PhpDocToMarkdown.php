<?php


namespace App\Library;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Storage;

class PhpDocToMarkdown
{
    const BADGE_TYPES = [
        'GET'    => 'secondary',
        'POST'   => 'primary',
        'PUT'    => 'success',
        'DELETE' => 'danger'
    ];

    /**
     * @param null $new_release
     * @return void
     * @throws \Exception
     */
    public static function generate($new_release = null)
    {
        $routes_list = self::getAppControllersRoutes();

        $doc_indexed_by_route_category_and_route_name = self::parseRoutesFileToGetPhpDoc();
        $processed_doc_indexed_by_route_category = self::processPhpDoc($doc_indexed_by_route_category_and_route_name);

        $routes_data_indexed_by_route_category = self::mergePhpDocDataWithRouteListData(
            $routes_list,
            $processed_doc_indexed_by_route_category
        );

        $markdown = self::generateMarkdownFromRoutesData($routes_data_indexed_by_route_category);

        self::writeMarkdownToFiles($new_release, $markdown);
    }

    /**
     * @return array
     */
    private static function parseRoutesFileToGetPhpDoc()
    {
        $api_route_file_names = Storage::disk('routes')->files('api');
        $api_route_file_paths = [Storage::disk('routes')->path('api.php')];
        foreach ($api_route_file_names as $name) {
            $api_route_file_paths[] = Storage::disk('routes')->path($name);
        }

        $regex_phpdoc_block = '(' . ' *\/\*\*\n(?: +\* ?(?:[[:alnum:]@ \p{P}\p{Sm}\/])*\n)+ +\*\/' . ')';
        $regex_named_route = '(?:Route::(?:get|post|delete|put)\(.+\)\n +->name\(\'([a-zA-Z.]+)\'\))';
        $final_regex = '/' . $regex_phpdoc_block . '\n *' . $regex_named_route . '/';

        $results = [];
        foreach ($api_route_file_paths as $route_file_path) {
            $file_results = [];
            $api_route_file_content = file_get_contents($route_file_path);

            $matches = [];
            preg_match_all('/' . $regex_named_route . '/', $api_route_file_content, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $route_name = $match[1];
                $file_results[$route_name] = ['title' => ucfirst(str_replace('.', ' ', $route_name))];
            }

            $matches = [];
            preg_match_all($final_regex, $api_route_file_content, $matches, PREG_SET_ORDER);
            foreach ($matches as $match) {
                $route_doc = $match[1];
                $route_name = $match[2];
                $file_results[$route_name] = $route_doc;
            }

            $file_index = ucfirst(rtrim(substr($route_file_path, strrpos($route_file_path, '/') + 1), ".php"));
            $results[$file_index] = $file_results;
        }

        return $results;
    }

    /**
     * @param $doc_indexed_by_route_category_and_route_name
     * @return array
     */
    private static function processPhpDoc($doc_indexed_by_route_category_and_route_name)
    {
        $processed_doc_indexed_by_route_category_and_route_name = [];
        foreach ($doc_indexed_by_route_category_and_route_name as $route_category => $doc_indexed_by_route_name) {
            $results = [];
            foreach ($doc_indexed_by_route_name as $route_name => $doc_block) {
                $processed_doc = self::parsePhpDocBlock($doc_block);
                $results[$route_name] = $processed_doc;
            }
            $processed_doc_indexed_by_route_category_and_route_name[$route_category] = $results;
        }

        return $processed_doc_indexed_by_route_category_and_route_name;
    }

    /**
     * @param $doc_block
     * @return array
     */
    private static function parsePhpDocBlock($doc_block)
    {
        if (is_array($doc_block)) {
            return $doc_block;
        }

        $regex_get_doc_lines = '/(?: +)\* ((?:(?:[[:alnum:]@ \p{P}\p{Sm}\/])(?!\*\/))+)/u';
        $matches = [];
        preg_match_all($regex_get_doc_lines, $doc_block, $matches);
        $doc_block_lines = $matches[1];

        $processed_doc = [];
        $multi_line_text_tag = '';
        $multi_line_text = '';
        foreach ($doc_block_lines as $doc_block_line) {
            $matches = [];
            preg_match('/(?:^@(\w+) ?)?(.*)/', $doc_block_line, $matches);
            $tag = $matches[1];
            $text = $matches[2];

            // If there is a tag, we start by record content in multiline variables if there is any. Then we fill
            // them with current tag and current content.
            if (!empty($tag)) {
                // See documentation inside the function appendMultilineVariablesContentIfAny()
                $processed_doc = self::appendMultilineVariablesContentIfAny(
                    $processed_doc,
                    $multi_line_text_tag,
                    $multi_line_text
                );
                // We define the the tag and its content as $multilines variables. We don't record it right now
                // in case the content is multi-line.
                $multi_line_text = $text;
                $multi_line_text_tag = $tag;
                // If there is content without tag, it means that line is part of a multi-line content tag. We concat
                // this line to $multilines variable, that already contains the beginning of the multi-line content.
            } elseif (empty($tag) && !empty($text)) {
                $multi_line_text .= ' ' . $text;
            }
        }
        // Once we parsed all lines, we need to record multi-line content tag if there is one in last position in the
        // block. We need to perform the same check as above on the existence of the tag.
        return self::appendMultilineVariablesContentIfAny(
            $processed_doc,
            $multi_line_text_tag,
            $multi_line_text
        );
    }

    /**
     * @param $processed_doc
     * @param $multi_line_text_tag
     * @param $multi_line_text
     * @return mixed
     */
    private static function appendMultilineVariablesContentIfAny($processed_doc, $multi_line_text_tag, $multi_line_text)
    {
        // If there's a tag and $multiline_tag variables is not empty, this means there is a tag with
        // content right before that needs to be recorded...
        if (!empty($multi_line_text_tag)) {
            // ...unless it's already recorded. In that case, we will create an array for this tag with all its
            // different content. If the array is already created, we just add the content to the array.
            if (Arr::has($processed_doc, $multi_line_text_tag)) {
                // The array is not created, so we put the content that was there before in an array with the
                // content we were trying to add.
                if (is_string($processed_doc[$multi_line_text_tag])) {
                    $processed_doc[$multi_line_text_tag] = [
                        $processed_doc[$multi_line_text_tag],
                        $multi_line_text
                    ];
                // The array is already created. We just add the new content.
                } elseif (is_array($processed_doc[$multi_line_text_tag])) {
                    $processed_doc[$multi_line_text_tag][] = $multi_line_text;
                }
            } else {
                $processed_doc[$multi_line_text_tag] = $multi_line_text;
            }
        }

        return $processed_doc;
    }

    /**
     * @param $routes_data_by_category
     * @return array
     * @throws \Exception
     */
    private static function generateMarkdownFromRoutesData($routes_data_by_category)
    {
        $getPathSectionLink = function ($path, $section = "") {
            $path = strtolower($path);
            return "/{{route}}/{{version}}/$path$section";
        };

        $markdown = [];
        $markdown['index'] = "- ## Jobello API documentation\n";
        $markdown['index'] .= "\t- [Intro](/{{route}}/{{version}}/intro)\n";
        $markdown['index'] .= "\t- [Routes](#)\n";

        foreach ($routes_data_by_category as $category => $routes_data_by_name) {
            $category_pretty = str_replace('_', ' ', $category);
            $index = "\t\t- [- $category_pretty]({$getPathSectionLink($category)})\n";
            $content = "# $category_pretty routes\n\n---\n\n";
            $table_of_contents = "";
            $anchor_index = 1;

            foreach ($routes_data_by_name as $route_name => $route_data) {
                if (!Arr::has($route_data, ['title', 'uri', 'method'])) {
                    throw new \Exception("Title, uri or method field is missing in $route_name, $category");
                }

                if (!$route_data['title'] || !$route_data['uri'] || !$route_data['method']) {
                    throw new \Exception("Title, uri or method field is empty in $route_name, $category");
                }

                $section_link = $getPathSectionLink($category, "#section-{$anchor_index}");
                $section_badge = RouteDataToMarkdown::getMarkdownContentMethod($route_data);
                $table_of_contents .= "- [$section_badge{$route_data['title']}]($section_link)\n";

                $content .= RouteDataToMarkdown::getMarkdownContentFromRouteData($route_name, $route_data, $anchor_index);
                $anchor_index++;
            }

            $markdown['index'] .= $index;
            $markdown[$category] = Arr::get($markdown, $category, "") . $table_of_contents . "\n" . $content;
        }

        return $markdown;
    }

    /**
     * @param $new_release
     * @param $markdown
     */
    private static function writeMarkdownToFiles($new_release, $markdown)
    {
        $docs_disk = Storage::disk('docs');
        $release = !is_null($new_release) ? $new_release : config('larecipe.versions.default');

        foreach ($markdown as $markdown_name => $markdown_content) {
            $markdown_name = strtolower($markdown_name);
            $md_file_path = "$release/$markdown_name.md";
            $docs_disk->put($md_file_path, $markdown_content);
        }
    }

    /**
     * @return array
     */
    private static function getAppControllersRoutes()
    {
        $all_routes_by_name = Route::getRoutes()->getRoutesByName();
        $app_controllers_routes = [];

        foreach ($all_routes_by_name as $name => $route) {
            /** @var $route \Illuminate\Routing\Route */
            // Only get routes from App\Http\Controllers
            if (stripos(get_class($route->getController()), 'App\Http\Controllers') === false) {
                continue;
            }

            // Remove method HEAD if there is one
            $methods = array_filter($route->methods, static function ($method) {
                return $method !== 'HEAD';
            });
            $method = current($methods);
            $uri = $route->uri();

            $app_controllers_routes[$name] = [
                'uri'    => '/' . $uri,
                'method' => $method
            ];
        }

        return $app_controllers_routes;
    }

    /**
     * @param $routes_list
     * @param $processed_doc_indexed_by_route_category
     * @return array
     */
    private static function mergePhpDocDataWithRouteListData($routes_list, $processed_doc_indexed_by_route_category)
    {
        $route_data_indexed_by_route_category = [];
        foreach ($processed_doc_indexed_by_route_category as $category => $processed_doc_indexed_by_route_name) {
            $route_data_indexed_by_route_name = [];
            foreach ($processed_doc_indexed_by_route_name as $route_name => $processed_doc) {
                $default_infos = Arr::get($routes_list, $route_name, []);
                $route_data_indexed_by_route_name[$route_name] = array_merge(
                    $default_infos,
                    $processed_doc
                );
            }
            $route_data_indexed_by_route_category[$category] = $route_data_indexed_by_route_name;
        }

        return $route_data_indexed_by_route_category;
    }
}
