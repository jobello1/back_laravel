<?php

namespace App\Library;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class RouteDataToMarkdown
{
    const BADGE_TYPES = [
        'GET'    => 'secondary',
        'POST'   => 'primary',
        'PUT'    => 'success',
        'DELETE' => 'danger'
    ];

    /**
     * Generate markdown of a route from its formatted data.
     *
     * @param $route_name
     * @param $route_data   : formatted data.
     * @param $anchor_index : integer that ties an anchor and its hyperlink
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public static function getMarkdownContentFromRouteData($route_name, $route_data, $anchor_index)
    {
        $final = self::getMarkdownContentHeader($route_name, $route_data, $anchor_index);
        $final .= self::getMarkdownContentDescription($route_data);
        $final .= self::getMarkdownContentPathParam($route_data);
        $final .= self::getMarkdownContentQueryParam($route_data);
        $final .= self::getMarkdownContentRuleClass($route_data) ?: self::getMarkdownContentBodyParam($route_data);
        $final .= self::getMarkdownContentResponseTransformer($route_data)
            ?: self::getMarkdownContentResponseJsonFile($route_data);
        $final .= self::getMarkdownContentError($route_data);

        $final .= "\n\n---\n\n";

        return $final;
    }

    /**
     * @param $route_name
     * @param $route_data
     * @param $anchor_index
     * @return string
     */
    public static function getMarkdownContentHeader($route_name, $route_data, $anchor_index)
    {
        $anchor = "<p id='section-{$anchor_index}'></p>";
        $method = self::getMarkdownContentMethod($route_data);
        $uri = self::getMarkdownContentUri($route_data);
        $title = self::getMarkdownContentTitle($route_data);
        $name = self::getMarkdownContentName($route_name);

        return $anchor . "\n" . $method . $uri
            . "\n<div class='title_n_badge'>" . $title . "</div>"
            . "\n<div class='id_route'>" . $name . "</div>\n";
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentMethod($route_data)
    {
        $badge_type = self::BADGE_TYPES[$route_data['method']];
        return "<span class='badge badge-$badge_type method-badge'>{$route_data['method']}</span>";
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentUri($route_data)
    {
        return "`{$route_data['uri']}`";
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentTitle($route_data)
    {
        return "<h2>{$route_data['title']}</h2>";
    }

    public static function getMarkdownContentName($route_name)
    {
        return "<code>{$route_name}</code>";
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentDescription($route_data)
    {
        if (!Arr::has($route_data, 'description')) {
            return "";
        }

        return "\n" . $route_data['description'];
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentPathParam($route_data)
    {
        if (!Arr::has($route_data, 'pathparam')) {
            return "";
        }

        $data = is_string($route_data['pathparam']) ? [$route_data['pathparam']] : $route_data['pathparam'];
        $pathparam_title = "#### Path parameters";
        $pathparam_table = "|Label|Description|\n|:|:|\n";
        $pathparam_table = self::generateMarkdownTableFromData($data, $pathparam_table);

        return "\n" . $pathparam_title . "\n\n" . $pathparam_table;
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentQueryParam($route_data)
    {
        if (!Arr::has($route_data, 'queryparam')) {
            return "";
        }

        $data = is_string($route_data['queryparam']) ? [$route_data['queryparam']] : $route_data['queryparam'];
        $queryparam_title = "#### Query parameters";
        $queryparam_table = "|Label|Description|\n|:|:|\n";
        $queryparam_table = self::generateMarkdownTableFromData($data, $queryparam_table);

        return "\n" . $queryparam_title . "\n\n" . $queryparam_table;
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentRuleClass($route_data)
    {
        if (!Arr::has($route_data, 'ruleclass')) {
            return "";
        }

        $line_parts = preg_split("/ : /", $route_data['ruleclass']);
        $rule_class = $line_parts[0];
        $model_from_class = str_replace("Store", "", $rule_class);
        $model_from_class = str_replace("Request", "", $model_from_class);
        $rule_model = count($line_parts) == 2 ? $line_parts[1] : $model_from_class;

        $rule_class_path = "App\Http\Requests\\" . $rule_class;
        $rule_model_path = "App\\" . $rule_model;

        if (!class_exists($rule_class_path) || !class_exists($rule_model_path)) {
            Log::warning("'{$route_data['title']}' route | RuleClass tag : class or model not found");
            return "";
        }

        $ruleclass_title = "#### Body parameters";
        $ruleclass_table = "|Label|Description|\n|:|:|\n";

        try {
            $ruleclass_instance = new $rule_class_path();
            $ruleclass_instance->setMethod($route_data['method']);
            $ruleclass_instance->request->add([strtolower($rule_model) => $rule_model_path::first()]);
            $rules = $ruleclass_instance->rules();
        } catch (\Exception $e) {
            Log::warning("'{$route_data['title']}' route | RuleClass tag : error while executing rules method");
            return "";
        }

        foreach ($rules as $param => $rule) {
            if (is_array($rule)) {
                $rule = implode("|", $rule);
            }
            $rule = str_replace('|', ", ", $rule);
            if (strpos($rule, "required") !== false) {
                $param = "<span class='required-param'>" . $param . "</span>";
            }
            $ruleclass_table .= "| $param\t| $rule\t|\n";
        }

        return "\n" . $ruleclass_title . "\n\n" . $ruleclass_table;
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentBodyParam($route_data)
    {
        if (!Arr::has($route_data, 'bodyparam')) {
            return "";
        }

        $data = is_string($route_data['bodyparam']) ? [$route_data['bodyparam']] : $route_data['bodyparam'];
        $bodyparam_title = "#### Body parameters";
        $bodyparam_table = "|Label|Description|\n|:|:|\n";
        $bodyparam_table = self::generateMarkdownTableFromData($data, $bodyparam_table);
        return "\n" . $bodyparam_title . "\n\n" . $bodyparam_table;
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentError($route_data)
    {
        if (!Arr::has($route_data, 'error')) {
            return "";
        }

        $data = is_string($route_data['error']) ? [$route_data['error']] : $route_data['error'];
        $error_title = "#### Errors";
        $error_table = "|Code|Description|\n|:|:|\n";
        $error_table = self::generateMarkdownTableFromData($data, $error_table, false);
        return "\n" . $error_title . "\n\n" . $error_table;
    }

    /**
     * @param $route_data
     * @return string
     */
    public static function getMarkdownContentResponseTransformer($route_data)
    {
        if (!Arr::has($route_data, 'responsetransformer')) {
            return "";
        }

        $line_parts = preg_split("/ : /", $route_data['responsetransformer']);
        $transformer_class = $line_parts[0];
        $model_from_class = str_replace("Transformer", "", $transformer_class);
        $transformer_model = count($line_parts) == 2 ? $line_parts[1] : $model_from_class;

        $class_path = "App\Transformers\\" . $transformer_class;
        $model_path = "App\\" . $transformer_model;

        if (!class_exists($class_path) || !class_exists($model_path)) {
            Log::warning("'{$route_data['title']}' route | ResponseTransformer tag : class or model not found");
            return "";
        }

        $model_object = $model_path::first();

        if (!$model_object) {
            Log::warning("'{$route_data['title']}' route | ResponseTransformer tag : "
                . "no model available to be used in transform method");
            return "";
        }

        if (defined("$model_path::WITH_RELATIONS")) {
            $model_object->loadMissing($model_path::WITH_RELATIONS);
        }

        $response_example = fractal($model_object, new $class_path());

        if (defined("$model_path::WITH_RELATIONS")) {
            $response_example = $response_example->parseIncludes($model_path::WITH_RELATIONS);
        }

        $response_example = $response_example->jsonSerialize();
        $responsetransformer_title = "#### Response example";
        $responsetransformer_content = json_encode($response_example, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

        return "\n" . $responsetransformer_title . "\n\n" .
            "<pre><code>" . $responsetransformer_content . "</code></pre>";
    }

    /**
     * @param $route_data
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    public static function getMarkdownContentResponseJsonFile($route_data)
    {
        if (!Arr::has($route_data, 'responsejsonfile')) {
            return "";
        }

        $docs_disk = Storage::disk('docs');
        $file_name = $route_data['responsejsonfile'];
        $file_path = "response_json_files/" . $file_name;

        if (!$docs_disk->has($file_path)) {
            Log::warning("'{$route_data['title']}' route | ResponseJsonFile tag : json file not found");
            return "";
        }

        $raw_json = $docs_disk->get($file_path);
        $json_as_object = json_decode($raw_json);
        $responsejsonfile_title = "#### Response example";
        $responsejsonfile_content = json_encode($json_as_object, JSON_PRETTY_PRINT|JSON_UNESCAPED_UNICODE);

        return "\n" . $responsejsonfile_title . "\n\n" .
            "<pre><code>" . $responsejsonfile_content . "</code></pre>";
    }

    /**
     * Generate a string interpreted as a table in markdown from formatted data.
     *
     * @param      $data           : formatted data.
     * @param      $table          : the generated table will be appended as a string to this variable.
     * @param bool $is_param_table : if true, somemarkdown parts of the data will be treated as required parameters. It
     *                             will lead to a different display once generated. Has no effect if data is not
     *                             formatted properly.
     * @return string
     */
    private static function generateMarkdownTableFromData($data, $table, $is_param_table = true)
    {
        foreach ($data as $line) {
            $line = str_replace('|', ", ", $line);
            $line_parts = preg_split("/ : /", $line);
            $desc = count($line_parts) == 2 ? $line_parts[1] : "-";
            if ($is_param_table && strpos($desc, "required") !== false) {
                $line_parts[0] = "<span class='required-param'>" . $line_parts[0] . "</span>";
            }
            $table .= "|{$line_parts[0]}\t|$desc\t|\n";
        }

        return $table;
    }
}
