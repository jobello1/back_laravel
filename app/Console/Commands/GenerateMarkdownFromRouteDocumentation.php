<?php

namespace App\Console\Commands;

use App\Library\PhpDocToMarkdown;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class GenerateMarkdownFromRouteDocumentation extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = "documentation:generate
        {--new_release= : If specified, the documentation will be generated as NEW_RELEASE. SemVer format expected.}
        {--force : Don\'t ask before overwriting current release. No effects if --new_release is specified}";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates markdown files from routes phpdoc';

    /**
     * Execute the console command.
     *
     * @return void
     * @throws \Exception
     */
    public function handle()
    {
        $new_release = $this->option('new_release');
        $force = $this->option('force');
        $current_release_number = config('larecipe.versions.default');
        $ask_text = "Current files from release $current_release_number will be overwritten. Continue ?";

        if ($new_release && !preg_match("/^(?:(?:[0-9]{1,2})(?:\.[0-9]{1,2}){0,2})$/", $new_release)) {
            $this->error("new_release value must be SemVer formatted, e.g. 'x.y.z' where x, y and z are integers");
            return;
        }

        if ($new_release || $force || $this->confirm($ask_text)) {
            PhpDocToMarkdown::generate($new_release);
            $this->info("Documentation generation job was started successfully !");
        } else {
            $this->warn("Aborted.");
        }

        if ($new_release) {
            $this->info("Don't forget to add new release $new_release in versions section in 'config/larecipe.php'.");
        }
    }
}
