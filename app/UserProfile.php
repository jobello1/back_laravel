<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserProfile extends Model
{
    const WITH_RELATIONS = ['descriptionNote', 'optionalNotes'];
    const ROLE_STUDENT = 'student';
    const ROLE_PROFESSIONAL = 'professional';
    const ROLES = [
        self::ROLE_STUDENT,
        self::ROLE_PROFESSIONAL
    ];

    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at', 'birth_date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 'lastname', 'role', 'city', 'post_code', 'job', 'formation', 'birth_date', 'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function descriptionNote()
    {
        return $this->hasOne(ProfileDescriptionNote::class);
    }

    public function optionalNotes()
    {
        return $this->hasMany(ProfileOptionalNote::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }
}
