<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    const WITH_RELATIONS = ['userProfile', 'answer', 'job'];
    /**
     * @var array
     */
    protected $dates = ['created_at', 'updated_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'content', 'job_id'];

    public function userProfile()
    {
        return $this->belongsTo(UserProfile::class);
    }

    public function job()
    {
        return $this->belongsTo(Job::class);
    }

    public function answer()
    {
        return $this->hasOne(Answer::class);
    }
}
