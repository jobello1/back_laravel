<?php

namespace App\Http\Requests;

class NoteRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required_if_post = $this->isMethod('POST') ? 'required|' : '';
        return [
            'title' => $required_if_post . 'string|max:200',
            'text'  => 'string|max:4000',
        ];
    }
}
