<?php

namespace App\Http\Requests;

class AnswerRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'content'   => 'required|string|max:4000',
        ];

        if ($this->isMethod('POST')) {
            $rules['user_profile_id'] = 'required|int|exists:user_profiles,id';
        }

        return $rules;
    }
}
