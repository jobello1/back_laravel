<?php

namespace App\Http\Requests;

use App\UserProfile;
use Carbon\Carbon;

class UserProfileRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $validation_array = [
            'firstname'  => ['string', 'max:150'],
            'lastname'   => ['string', 'max:150'],
            'birth_date' => ['date', 'before:' . Carbon::now()->toDateString()],
        ];

        $user_role = $this->user()->profile->role;

        if ($user_role === UserProfile::ROLE_PROFESSIONAL) {
            $validation_array['job'] = ['string', 'max:150'];
            $validation_array['city'] = ['string', 'max:150'];
            $validation_array['post_code'] = ['string', 'regex:/[0-9]{5}/'];
        } else if ($user_role === UserProfile::ROLE_STUDENT) {
            $validation_array['formation'] = ['string', 'max:150'];
        }

        return $validation_array;
    }
}
