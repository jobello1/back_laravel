<?php

namespace App\Http\Requests;

class QuestionRequest extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $required_if_post = $this->isMethod('POST') ? 'required|' : '';
        return [
            'title'     => $required_if_post . 'string|max:200',
            'content'   => 'string|max:4000',
            'job_id'    => $required_if_post . 'int|exists:jobs,id',
        ];
    }
}
