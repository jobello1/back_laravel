<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class isAuthorized
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param                          $user_model_binding
     * @return mixed
     */
    public function handle($request, Closure $next, $user_model_binding)
    {
        $resource_owner = $request->route()->parameter($user_model_binding);
        if (!$resource_owner) {
            throw new NotFoundHttpException('Could not find the user associated with the requested model');
        }

        if (!($user_to_grant = $request->user())) {
            throw new NotFoundHttpException('Could not find the user associated with the request');
        }

        if (!Gate::forUser($user_to_grant)->allows('isOwner', $resource_owner)) {
            throw new AccessDeniedHttpException('This user is not allowed to perform such action.');
        }

        return $next($request);
    }
}
