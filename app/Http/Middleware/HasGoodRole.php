<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class HasGoodRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     * @param                          $role_to_have
     * @return mixed
     */
    public function handle($request, Closure $next, $role_to_have)
    {
        if (!($user_to_grant = $request->user())) {
            throw new NotFoundHttpException('Could not find the user associated with the request');
        }

        if (!Gate::forUser($user_to_grant)->allows('hasGoodRole', $role_to_have)) {
            throw new AccessDeniedHttpException('This user has not the requested role.');
        }

        return $next($request);
    }
}
