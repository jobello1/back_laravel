<?php

namespace App\Http\Controllers;

use App\Http\Requests\QuestionRequest;
use App\Job;
use App\Question;
use App\Transformers\QuestionTransformer;
use App\User;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    /**
     * @param Request $request
     * @param User    $user
     * @return \Spatie\Fractal\Fractal
     */
    public function indexByUser(Request $request, User $user)
    {
        $questions = $user->profile->questions;

        return fractal($questions, new QuestionTransformer());
    }

    /**
     * @param Request $request
     * @param Job     $job
     * @return \Spatie\Fractal\Fractal
     */
    public function indexByJob(Request $request, Job $job)
    {
        $request->validate([
            'per_page' => 'int|min:1'
        ]);
        $questions = $job->questions()->paginate($request->input('per_page', 15));

        return fractal($questions, new QuestionTransformer())
            ->parseExcludes('job');
    }

    /**
     * @param QuestionRequest $request
     * @param User            $user
     * @return \Spatie\Fractal\Fractal
     */
    public function store(QuestionRequest $request, User $user)
    {
        $question = $user->profile->questions()->create($request->validated());

        return fractal($question, new QuestionTransformer());
    }

    /**
     * @param Request  $request
     * @param Question $question
     * @return \Spatie\Fractal\Fractal
     */
    public function show(Request $request, Question $question)
    {
        return fractal($question, new QuestionTransformer());
    }

    /**
     * @param QuestionRequest $request
     * @param Question        $question
     * @return \Spatie\Fractal\Fractal
     */
    public function update(QuestionRequest $request, Question $question)
    {
        $question->update($request->validated());

        return fractal($question, new QuestionTransformer());
    }

    /**
     * @param Request  $request
     * @param Question $question
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \Exception
     */
    public function destroy(Request $request, Question $question)
    {
        $question->delete();

        return response('', 204);
    }
}
