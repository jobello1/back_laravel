<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserProfileRequest;
use App\Transformers\UserProfileTransformer;
use App\User;
use App\UserProfile;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function myProfile(Request $request)
    {
        return fractal($request->user()->profile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }

    public function show(Request $request, User $user)
    {
        return fractal($user->profile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }

    public function update(UserProfileRequest $request, User $user)
    {
        $user->profile()->update($request->validated());

        return fractal($user->profile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }
}
