<?php

namespace App\Http\Controllers;

use App\Job;
use App\Transformers\JobTransformer;
use Illuminate\Http\Request;

class JobController extends Controller
{
    public function index()
    {
        return fractal(Job::all(), new JobTransformer())
            ->parseIncludes('linkedJobs');
    }
}
