<?php

namespace App\Http\Controllers;

use App\Answer;
use App\Http\Requests\AnswerRequest;
use App\Question;
use App\Transformers\QuestionTransformer;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

class AnswerController extends Controller
{
    public function store(AnswerRequest $request, Question $question)
    {
        if ($question->answer) {
            throw new BadRequestHttpException('This question has an answer already');
        }
        $answer = $question->answer()->create($request->validated());

        return fractal($answer->question, new QuestionTransformer());
    }

    public function update(AnswerRequest $request, Answer $answer)
    {
        $answer->update($request->validated());

        return fractal($answer->question, new QuestionTransformer());
    }

    public function destroy(Request $request, Answer $answer)
    {
        $answer->delete();

        return response('', 204);
    }
}
