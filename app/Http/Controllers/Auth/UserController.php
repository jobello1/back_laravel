<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Transformers\UserTransformer;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function me(Request $request)
    {
        return fractal($request->user(), new UserTransformer())
            ->respond();
    }
}
