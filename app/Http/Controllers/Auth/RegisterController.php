<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Traits\ProxyToken;
use App\User;
use App\UserProfile;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, ProxyToken;

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $validation_array = [
            'firstname' => ['required', 'string', 'max:150'],
            'lastname'  => ['required', 'string', 'max:150'],
            'role'      => ['required', Rule::in(UserProfile::ROLES)],
            'birth_date' => ['required', 'date', 'before:' . Carbon::now()->toDateString()],
            'email' => ['required', 'string', 'email', 'max:190', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
            // password_confirmation is needed since password has 'confirmed' rule above
        ];
        if (array_key_exists('role', $data) && $data['role'] === UserProfile::ROLE_PROFESSIONAL) {
            $validation_array['job'] = ['required', 'string', 'max:150'];
        }
        return Validator::make($data, $validation_array);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $user = DB::transaction(function () use ($data){
            $user = User::create([
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            $user_profile = $user->profile()->create([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'role' => $data['role'],
                'job' => array_key_exists('job', $data) ? $data['job'] : null,
                'birth_date' => $data['birth_date'],
            ]);

            $user_profile->descriptionNote()->create([
                'title' => 'À propos de ' . $user_profile->firstname,
            ]);

            return $user;
        });

        if (!$user) {
            throw new UnprocessableEntityHttpException('Wrong data provided');
        }

        return $user;
    }

    /**
     * The user has been registered.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function registered(Request $request, $user)
    {
        $params = [
            'username'      => $user->email,
            'password'      => $request->password,
            'client_id'     => config('oauth.client_id'),
            'client_secret' => config('oauth.client_secret')
        ];
        return $this::getToken($request, 'password', $params);
    }
}
