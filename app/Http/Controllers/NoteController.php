<?php

namespace App\Http\Controllers;

use App\Http\Requests\NoteRequest;
use App\ProfileOptionalNote;
use App\Transformers\UserProfileTransformer;
use App\User;
use App\UserProfile;

class NoteController extends Controller
{
    public function modifyDescriptionNote(NoteRequest $request, User $user)
    {
        $user->profile->descriptionNote()->update($request->validated());

        return fractal($user->profile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }

    public function addOptionalNote(NoteRequest $request, User $user)
    {
        $user->profile->optionalNotes()->create($request->validated());

        return fractal($user->profile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }

    public function removeOptionalNote(ProfileOptionalNote $note)
    {
        $note->delete();

        return fractal($note->userProfile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }

    public function modifyOptionalNote(NoteRequest $request, ProfileOptionalNote $note)
    {
        $note->update($request->validated());

        return fractal($note->userProfile, new UserProfileTransformer())
            ->parseIncludes(UserProfile::WITH_RELATIONS);
    }
}
